REST-API
========

REST API for managing BMP evaluation data

Uses Flask, SQLAlchemy and various libraries

Using anaconda (preferred method):

- $ conda create -n evaluate_it python=2.7.13 anaconda # create venv named evaluate_it
- $ source activate evaluate_it # activate venv
- $ while read requirement; do conda install --yes $requirement; done < rest_api/requirements.txt
- $ pip install -r requirements.txt #(to install requirements not included in conda repo)


Usage:

* Create database:
$./db_create.py

* Run RESTful web API:
$./api.py

Fabric build usage:

- $ fab --list 



